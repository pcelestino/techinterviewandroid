package com.joyjet.techinterviewandroid.api

import com.joyjet.techinterviewandroid.models.Category
import io.reactivex.Observable
import retrofit2.http.GET

interface Api {
    @GET("mobile-test-one.json")
    fun getCategories(): Observable<List<Category>>
}