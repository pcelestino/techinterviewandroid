package com.joyjet.techinterviewandroid.ui.adapters

import android.support.annotation.LayoutRes
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.joyjet.techinterviewandroid.R
import com.joyjet.techinterviewandroid.extensions.setImage
import com.joyjet.techinterviewandroid.models.CategoryImage

class CategoryImageAdapter constructor(
        @LayoutRes layoutResId: Int,
        data: List<CategoryImage>) :
        BaseQuickAdapter<CategoryImage, BaseViewHolder>(layoutResId, data) {

    override fun convert(helper: BaseViewHolder, item: CategoryImage) {
        helper.setImage(R.id.category_image, item.url)
    }
}