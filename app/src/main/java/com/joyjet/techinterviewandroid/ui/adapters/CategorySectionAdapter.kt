package com.joyjet.techinterviewandroid.ui.adapters

import android.support.annotation.LayoutRes
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ImageButton
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseSectionQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.joyjet.techinterviewandroid.R
import com.joyjet.techinterviewandroid.models.CategoryImage
import com.joyjet.techinterviewandroid.models.CategorySection

/**
 * Same as QuickAdapter#QuickAdapter(Context,int) but with
 * some initialization data.
 * @param sectionHeadResId The section head layout id for each item
 * *
 * @param layoutResId      The layout resource id of each item.
 * *
 * @param data             A new list is created out of this one to avoid mutable list
 */
class CategorySectionAdapter constructor(
        @LayoutRes layoutResId: Int,
        @LayoutRes sectionHeadResId: Int,
        data: List<CategorySection>?) :
        BaseSectionQuickAdapter<CategorySection, BaseViewHolder>(layoutResId, sectionHeadResId, data) {

    override fun convertHead(helper: BaseViewHolder, item: CategorySection) {
        helper.setText(R.id.category, item.header)
    }

    override fun convert(helper: BaseViewHolder, item: CategorySection) {
        val categoryItem = item.t
        helper.setText(R.id.category_item_title, categoryItem.title)
        helper.setText(R.id.category_item_description, categoryItem.description)
        helper.setListCategoryImage(categoryItem.gallery)
        helper.setListCategoryImageNavButtons(categoryItem.gallery)
    }

    private fun BaseViewHolder.setListCategoryImage(categoryImages: List<CategoryImage>) {
        val listCategoryImages: RecyclerView = this.getView(R.id.list_category_images)
        listCategoryImages.layoutManager = LinearLayoutManager(this.itemView.context, LinearLayoutManager.HORIZONTAL, false)
        listCategoryImages.setHasFixedSize(true)
        val sectionAdapter = CategoryImageAdapter(R.layout.item_list_category_image, categoryImages)
        listCategoryImages.adapter = sectionAdapter

        sectionAdapter.onItemChildClickListener = BaseQuickAdapter.OnItemChildClickListener { _, _, position ->
            Log.d("SECTION CHILD CLICK", "SECTION OnItemChildClickListener: " + position.toString())
            Log.d("ADAPTER POSITION", this.adapterPosition.toString())
            Log.d("LAYOUT POSITION", this.layoutPosition.toString())

        }

        sectionAdapter.onItemClickListener = BaseQuickAdapter.OnItemClickListener { _, _, position ->
            Log.d("SECTION CLICK", "SECTION OnItemChildClickListener: " + position.toString())
            Log.d("ADAPTER POSITION", this.adapterPosition.toString())
            Log.d("LAYOUT POSITION", this.layoutPosition.toString())
        }
    }

    private fun BaseViewHolder.setListCategoryImageNavButtons(categoryImages: List<CategoryImage>) {
        val listCategoryImages: RecyclerView = this.getView(R.id.list_category_images)
        val leftCategoryButton: ImageButton = this.getView(R.id.left_category_image_button)
        val rightCategoryButton: ImageButton = this.getView(R.id.right_category_image_button)

        val linearLayoutManager = listCategoryImages.layoutManager as LinearLayoutManager

        leftCategoryButton.setOnClickListener {
            val lastItemPosition = getLastItemPosition(linearLayoutManager)

            navLeft(lastItemPosition, listCategoryImages)

            checkNavLeftVisibilityState(lastItemPosition, rightCategoryButton, leftCategoryButton)
        }

        rightCategoryButton.setOnClickListener {
            val firstItemPosition = getFirstItemPosition(linearLayoutManager)

            navRight(firstItemPosition, categoryImages, listCategoryImages)

            checkNavRightVisibilityState(firstItemPosition, categoryImages, leftCategoryButton, rightCategoryButton)
        }
    }

    private fun getFirstItemPosition(linearLayoutManager: LinearLayoutManager) =
            linearLayoutManager.findFirstVisibleItemPosition() + 1

    private fun getLastItemPosition(linearLayoutManager: LinearLayoutManager) =
            linearLayoutManager.findLastVisibleItemPosition() - 1

    private fun checkNavRightVisibilityState(firstItemPosition: Int, categoryImages: List<CategoryImage>, leftCategoryButton: ImageButton, rightCategoryButton: ImageButton) {
        if (firstItemPosition < categoryImages.size) {
            leftCategoryButton.visibility = View.VISIBLE
        }
        if (firstItemPosition == categoryImages.size - 1) {
            rightCategoryButton.visibility = View.GONE
        }
    }

    private fun checkNavLeftVisibilityState(lastItemPosition: Int, rightCategoryButton: ImageButton, leftCategoryButton: ImageButton) {
        if (lastItemPosition >= 0) {
            rightCategoryButton.visibility = View.VISIBLE
        }
        if (lastItemPosition <= 0) {
            leftCategoryButton.visibility = View.GONE
        }
    }

    private fun navRight(firstItemPosition: Int, categoryImages: List<CategoryImage>, listCategoryImages: RecyclerView) {
        if (firstItemPosition < categoryImages.size) {
            listCategoryImages.smoothScrollToPosition(firstItemPosition)
        }
    }

    private fun navLeft(lastItemPosition: Int, listCategoryImages: RecyclerView) {
        if (lastItemPosition >= 0) {
            listCategoryImages.smoothScrollToPosition(lastItemPosition)
        }
    }
}