package com.joyjet.techinterviewandroid.ui.fragments


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chad.library.adapter.base.BaseQuickAdapter

import com.joyjet.techinterviewandroid.R
import com.joyjet.techinterviewandroid.api.Api
import com.joyjet.techinterviewandroid.database.CategoryDao
import com.joyjet.techinterviewandroid.extensions.asCategorySections
import com.joyjet.techinterviewandroid.models.Category
import com.joyjet.techinterviewandroid.ui.adapters.CategorySectionAdapter
import dagger.android.support.AndroidSupportInjection
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_list_category.view.*
import javax.inject.Inject



class ListCategoryFragment : Fragment() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var categoryDao: CategoryDao

//    @Inject
//    lateinit var categoryItemDao: CategoryItemDao
//
//    @Inject
//    lateinit var categoryImageDao: CategoryImageDao

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_list_category, container, false)
        loadCategories(view.list_category)
        return view
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun loadCategories(listCategory: RecyclerView) {
        val adapter = setupListCategory(listCategory)

        handleClickEvents(adapter)

        api.getCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    insertAllCategories(it)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe {
                                categoryDao.getAll()
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe { categories ->
                                            Log.d("LISTA DE CATEGORIAS", categories.toString())
                                            adapter.setNewData(it.asCategorySections())
                                        }
                            }
                }
    }

    private fun insertAllCategories(categories: List<Category>): Completable {

        return Completable.fromCallable {
            categoryDao.insertAll(categories)
//            categories.forEach { category ->
//                categoryDao.insert(category)
//                category.items.forEach { categoryItem ->
//                    val copiedCategoryItem = categoryItem.copy(category = category.category)
//                    categoryItemDao.insert(copiedCategoryItem)
//                }
//            }
//            val categoryItems = categoryItemDao.getAll().blockingGet()
//            categoryItems.forEach { categoryItem ->
//                categoryItem.gallery.forEach { categoryImage ->
//                    val copiedCategoryImage = categoryImage.copy(categoryItemId = categoryItem.id)
//                    categoryImageDao.insert(copiedCategoryImage)
//                }
//            }
        }
    }

    private fun setupListCategory(listCategory: RecyclerView): CategorySectionAdapter {
        listCategory.layoutManager = LinearLayoutManager(this.context)
        listCategory.setHasFixedSize(true)

        val emptyView = layoutInflater.inflate(R.layout.empty_list_category, listCategory.parent as ViewGroup, false)

        val sectionAdapter = CategorySectionAdapter(
                R.layout.item_list_category,
                R.layout.section_list_category,
                null
        )
        listCategory.adapter = sectionAdapter
        sectionAdapter.emptyView = emptyView
        sectionAdapter.setEmptyView(R.layout.empty_list_category, listCategory.parent as ViewGroup)
        return sectionAdapter
    }

    private fun handleClickEvents(adapter: CategorySectionAdapter) {
        adapter.onItemChildClickListener = BaseQuickAdapter.OnItemChildClickListener { _, _, position ->
            Log.d("TESTE", "OnItemChildClickListener: " + position.toString())
        }

        adapter.onItemClickListener = BaseQuickAdapter.OnItemClickListener { _, _, position ->
            Log.d("TESTE", "onItemClick: " + position.toString())
        }
    }
}
