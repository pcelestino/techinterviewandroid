package com.joyjet.techinterviewandroid.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(
        tableName = "category_image"
)
@ForeignKey(entity = CategoryItem::class,
        parentColumns = ["id"],
        childColumns = ["category_item_id"])
data class CategoryImage(

        @Transient
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Long = 0,

        @ColumnInfo(name = "category_item_id")
        var categoryItemId: Long = 0,

        @ColumnInfo(name = "url")
        var url: String = ""
)