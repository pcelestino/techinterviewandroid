package com.joyjet.techinterviewandroid.models

import android.arch.persistence.room.*
import com.squareup.moshi.Json

@Entity(
        tableName = "category_item"
)
@ForeignKey(entity = Category::class,
        parentColumns = ["category"],
        childColumns = ["category"])
data class CategoryItem(

        @Transient
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Long = 0,

        @ColumnInfo(name = "category")
        var category: String = "",

        @Json(name = "title")
        @ColumnInfo(name = "title")
        var title: String = "",

        @Json(name = "description")
        @ColumnInfo(name = "description")
        var description: String = "",

        @Json(name = "galery")
        @Ignore
        var gallery: List<CategoryImage> = listOf()
)