package com.joyjet.techinterviewandroid.models

import com.chad.library.adapter.base.entity.SectionEntity

class CategorySection : SectionEntity<CategoryItem> {
    constructor(isHeader: Boolean, header: String) : super(isHeader, header)
    constructor(t: CategoryItem) : super(t)
}