package com.joyjet.techinterviewandroid.models

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.squareup.moshi.Json
import java.io.Serializable

@Entity(
        tableName = "category"
)
data class Category(

        @Json(name = "category")
        @PrimaryKey
        @ColumnInfo(name = "category")
        var category: String = "",

        @Json(name = "items")
        @Ignore
        var items: List<CategoryItem> = listOf()

)