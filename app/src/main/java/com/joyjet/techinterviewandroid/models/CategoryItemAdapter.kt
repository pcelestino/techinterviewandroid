package com.joyjet.techinterviewandroid.models

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson

class CategoryItemAdapter {

    @FromJson
    fun fromJson(reader: JsonReader): CategoryItem? {
        var title = ""
        var description = ""
        var gallery: List<CategoryImage> = listOf()

        reader.beginObject()
        while (reader.hasNext()) {
            val name = reader.nextName()
            when (name) {
                "title" -> title = reader.nextString()
                "description" -> description = reader.nextString()
                "galery" -> gallery = readCategoryImages(reader)
            }
        }
        reader.endObject()

        return CategoryItem(title = title, description = description, gallery = gallery)
    }

    private fun readCategoryImages(reader: JsonReader): List<CategoryImage> {
        val categoryImages: MutableList<CategoryImage> = mutableListOf()
        reader.beginArray()
        while (reader.hasNext()) {
            categoryImages.add(CategoryImage(url = reader.nextString()))
        }
        reader.endArray()
        return categoryImages
    }

    @ToJson
    fun toJson(writer: JsonWriter, value: CategoryItem?) {
        value?.let {
            writer.beginObject()
            writer.name("title").value(it.title)
            writer.name("description").value(it.description)
            writeStringArray(writer, it.gallery)
            writer.endObject()
        }
    }

    private fun writeStringArray(writer: JsonWriter, categoryImages: List<CategoryImage>) {
        writer.beginArray()
        for (categoryImage: CategoryImage in categoryImages) {
            writer.name("galery").value(categoryImage.url)
        }
        writer.endArray()
    }
}