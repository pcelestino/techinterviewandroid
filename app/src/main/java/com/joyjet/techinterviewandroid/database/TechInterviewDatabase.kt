package com.joyjet.techinterviewandroid.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.joyjet.techinterviewandroid.models.Category

@Database(entities = [Category::class], version = 1)
abstract class TechInterviewDatabase : RoomDatabase() {
    abstract fun categoryDao(): CategoryDao
}