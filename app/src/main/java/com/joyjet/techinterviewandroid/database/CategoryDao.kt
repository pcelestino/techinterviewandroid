package com.joyjet.techinterviewandroid.database

import android.arch.persistence.room.*
import com.joyjet.techinterviewandroid.models.Category
import io.reactivex.Single

@Dao
interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(category: Category)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(categories: List<Category>)

    @Update
    fun update(category: Category)

    @Update
    fun updateAll(categories: List<Category>)

    @Query("select * from category where category = :category limit 1")
    fun get(category: String): Single<Category>

    @Query("select * from category")
    fun getAll(): Single<List<Category>>

    @Delete
    fun delete(category: Category)

    @Query("delete from category")
    fun deleteAll()
}