package com.joyjet.techinterviewandroid.extensions

import com.joyjet.techinterviewandroid.models.Category
import com.joyjet.techinterviewandroid.models.CategorySection

fun List<Category>.asCategorySections(): List<CategorySection> {
    val categorySections: MutableList<CategorySection> = mutableListOf()

    this.forEach {
        categorySections.add(CategorySection(true, it.category))
        it.items.forEach { categorySections.add(CategorySection(it)) }
    }

    return categorySections
}