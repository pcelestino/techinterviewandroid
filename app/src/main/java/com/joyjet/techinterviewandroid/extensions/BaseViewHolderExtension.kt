package com.joyjet.techinterviewandroid.extensions

import android.support.annotation.IdRes
import android.widget.ImageView
import com.chad.library.adapter.base.BaseViewHolder

fun BaseViewHolder.setImage(@IdRes viewId: Int, url: String) {
    val imageView = this.getView<ImageView>(viewId)
    imageView.setImage(url)
}