package com.joyjet.techinterviewandroid.extensions

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

inline fun <reified T : Activity> Activity.startActivity() {
    startActivity(Intent(this, T::class.java))
}

inline fun <reified T : Activity> Application.startActivity() {
    startActivity(Intent(this, T::class.java))
}

fun AppCompatActivity.showFragment(@IdRes containerViewIdRes: Int, fragment: Fragment, stacks: Boolean) {
    supportFragmentManager.beginTransaction()
            .replace(containerViewIdRes, fragment)
            .also {
                if (stacks) it.addToBackStack(null)
            }
            .commit()
}