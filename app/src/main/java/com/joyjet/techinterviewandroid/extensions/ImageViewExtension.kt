package com.joyjet.techinterviewandroid.extensions

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable
import android.widget.ImageView
import com.joyjet.techinterviewandroid.R
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator

fun ImageView.setImage(url: String) {
    Picasso.get()
            .load(url)
            .withProgress(this.context)
            .error(R.drawable.error)
            .into(this)
}

private fun RequestCreator.withProgress(context: Context): RequestCreator {
    val circularProgressDrawable = CircularProgressDrawable(context)
    circularProgressDrawable.setColorSchemeColors(ContextCompat.getColor(context, R.color.colorAccent))
    circularProgressDrawable.setStyle(CircularProgressDrawable.LARGE)
    circularProgressDrawable.start()
    return this.placeholder(circularProgressDrawable)
}