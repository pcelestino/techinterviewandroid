package com.joyjet.techinterviewandroid.di.modules

import com.joyjet.techinterviewandroid.ui.activities.MainActivity
import com.joyjet.techinterviewandroid.ui.fragments.ListCategoryFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeListCategoryFragment(): ListCategoryFragment
}