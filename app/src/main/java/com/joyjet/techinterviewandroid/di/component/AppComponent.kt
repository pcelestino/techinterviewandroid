package com.joyjet.techinterviewandroid.di.component

import com.joyjet.techinterviewandroid.TechInterviewApplication
import com.joyjet.techinterviewandroid.di.modules.AppModule
import com.joyjet.techinterviewandroid.di.modules.BuildersModule
import com.joyjet.techinterviewandroid.di.modules.NetModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidInjectionModule::class,
            BuildersModule::class,
            AppModule::class,
            NetModule::class
        ]
)
interface AppComponent {
    fun inject(app: TechInterviewApplication)
}