package com.joyjet.techinterviewandroid.di.modules

import android.app.Application
import android.arch.persistence.room.Room
import com.joyjet.techinterviewandroid.database.CategoryDao
import com.joyjet.techinterviewandroid.database.TechInterviewDatabase
import dagger.Module
import dagger.Provides
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class AppModule(val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideTechInterviewDatabase(app: Application): TechInterviewDatabase = Room.databaseBuilder(app,
            TechInterviewDatabase::class.java,
            "tech_interview.db")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideCategoryDao(techInterviewDatabase: TechInterviewDatabase): CategoryDao {
        // Clear database
        Completable.fromCallable { techInterviewDatabase.clearAllTables() }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()

        return techInterviewDatabase.categoryDao()
    }

//    @Provides
//    @Singleton
//    fun provideCategoryItemDao(techInterviewDatabase: TechInterviewDatabase): CategoryItemDao =
//        techInterviewDatabase.categoryItemDao()
//
//    @Provides
//    @Singleton
//    fun provideCategoryImageDao(techInterviewDatabase: TechInterviewDatabase): CategoryImageDao =
//            techInterviewDatabase.categoryImageDao()
}