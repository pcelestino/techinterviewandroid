package com.joyjet.techinterviewandroid.delegates

import android.support.v4.app.Fragment

interface NavigationDelegate {
    fun show(fragment: Fragment, stacks: Boolean)
    fun back()
}